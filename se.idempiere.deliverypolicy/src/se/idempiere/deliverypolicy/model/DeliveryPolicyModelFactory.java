package se.idempiere.deliverypolicy.model;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

public class DeliveryPolicyModelFactory implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
		
		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrderLine.Table_Name))
			return se.idempiere.deliverypolicy.model.MOrderLine.class;
		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrder.Table_Name))
			return se.idempiere.deliverypolicy.model.MOrder.class;
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {

		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrderLine.Table_Name))
			return new se.idempiere.deliverypolicy.model.MOrderLine(Env.getCtx(), Record_ID, trxName);
		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrder.Table_Name))
			return new se.idempiere.deliverypolicy.model.MOrder(Env.getCtx(), Record_ID, trxName);		
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		
		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrderLine.Table_Name))
			return new se.idempiere.deliverypolicy.model.MOrderLine(Env.getCtx(), rs, trxName);
		if (tableName.equalsIgnoreCase(se.idempiere.deliverypolicy.model.MOrder.Table_Name))
			return new se.idempiere.deliverypolicy.model.MOrder(Env.getCtx(), rs, trxName);
		
		return null;
	}

}
