package se.idempiere.deliverypolicy;

import org.compiere.util.CLogger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	private static BundleContext context;
	public static CLogger log = CLogger.getCLogger("se.idempiere.premium");
	public static final String SALES_ORDERLINE_ALLOCATION = "SALES_ORDERLINE_ALLOCATION";

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		boolean result = checkDatabase();
		if (!result) {
			log.warning("Database not up to date for this plugin.");
		}
		log.info("iDempiere Delivery Policy plugin started.");
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

	/**
	 * 
	 * Checks that necessary database columns exist
	 * 
	 * @return	true if everything is correct.
	 * 
	 */
	private boolean checkDatabase() {

		log.info("Here we are supposed to check that all columns exist in database");
		
		// Check that our process InOutGenerate is used.
		
		// Column needed
		// C_OrderLine.QtyAllocated
		
		return true;
	}


}
