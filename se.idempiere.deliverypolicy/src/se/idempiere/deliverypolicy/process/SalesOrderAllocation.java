package se.idempiere.deliverypolicy.process;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.CPreparedStatement;
import org.compiere.util.DB;
import org.compiere.util.Env;

import se.idempiere.deliverypolicy.Activator;
import se.idempiere.deliverypolicy.model.MOrderLine;


public class SalesOrderAllocation extends SvrProcess {
	
	private int	m_warehouseId;

	/**
	 * Class to hold return information about a product
	 * 
	 * @author daniel.tamm
	 *
	 */
	public static class StockInfo {
		
		public int			warehouseId;
		public int			productId;
		public BigDecimal	qtyOnHand;
		public BigDecimal	qtyAvailable;
		public BigDecimal	qtyReserved;
		public BigDecimal	qtyAllocated;
		
		public StockInfo() {}
		
	}
	
	@Override
	protected void prepare() {
		
		m_warehouseId = 1000000; // TODO: Should be mandatory in the process definition
		
        ProcessInfoParameter[] para = getParameter();
        for (int i = 0; i < para.length; i++) {
            String name = para[i].getParameterName();
            if (para[i].getParameter() == null);
            else if (name.equals("M_Warehouse_ID")) {
            	m_warehouseId = para[i].getParameterAsInt();
            } else {
                log.log(Level.SEVERE, "Unknown Parameter: " + name);
            }
        }
		
	}
	
	@Override
	protected String doIt() throws Exception {
		try {
			String result = runSalesOrderAllocation(this, log, getCtx(), m_warehouseId, 0, 0, get_TrxName());
			return(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	/**
	 * Runs sales order allocation. This runs on all open sales orders.
	 * 
	 * @param	log				Logger to use for logging
	 * @param	ctx				Context
	 * @param	warehouseID		Warehouse id
	 * @param	orderID			If this is supplied (can be zero) this order is also considered
	 * 							for allocation. This mechanism exists because the order might be transferring from
	 * 							status drafted when this function is called.
	 * @param 	trxName			Transaction.	
	 * @return
	 * @throws Exception
	 */
	public static String runSalesOrderAllocation(SvrProcess p, CLogger log, Properties ctx, int warehouseID, int orderID, int productID, String trxName) throws Exception {
		List<MOrderLine> lines;
		MOrderLine line;
		BigDecimal lineAllocate;
		BigDecimal toAllocate;
		BigDecimal onHand;
		BigDecimal allocated;
		BigDecimal qtyAvailable;
		BigDecimal willAllocate;
		StockInfo si;
		
		// Make sure we have settings that needs allocation
		boolean salesOrderAllocationEnabled = MSysConfig.getBooleanValue(Activator.SALES_ORDERLINE_ALLOCATION, false, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx));
		if (!salesOrderAllocationEnabled) {
			return "The current delivery policy of the client doesn't use allocation.";
		}

		List<StockInfo> products = SalesOrderAllocation.getProductsToAllocate(warehouseID, orderID, productID, trxName);
		
		if (products.size()==0) {
			log.info("There are no products to allocate.");
		}
		
		/** Iterate through all products to allocate */
		for (Iterator<StockInfo> it = products.iterator(); it.hasNext();) {
			
			MProduct product = null;
			si = it.next();
			
			// Only try to allocate items that are available or needs to be deallocated.			
			if (si.qtyOnHand.signum()==1 || si.qtyAllocated.doubleValue()>si.qtyReserved.doubleValue()) {
				// Get all order lines to allocate, these are lines where not all are delivered and not all
				// reserved are allocated.
				
				// Send variable as reference so this does not have to be calculated separately. (Using array otherwise it will not be sent as reference)
				BigDecimal[] needsToBeToAllocated = {Env.ZERO};				
				lines = SalesOrderAllocation.getOrderLinesToAllocate(si.productId, orderID, needsToBeToAllocated, trxName);

				// Check if there are any lines to allocate
				// and create a log.
				if (lines.size()>0) {
					product = lines.get(0).getProduct();
					line = lines.get(0);
					// In this case, available is the same as all non allocated
					qtyAvailable = si.qtyOnHand.subtract(si.qtyAllocated.min(si.qtyReserved));
					onHand = si.qtyOnHand;
					// TO allocate if every MStorage number is correct
					// = Min (qtyOnHand - qtyAllocated, qtyReserved - qtyAllocated)
					toAllocate = si.qtyOnHand.subtract(si.qtyAllocated).min(si.qtyReserved.subtract(si.qtyAllocated));
					// To allocate can be negative if we're lowering the number ordered.
					if (toAllocate.signum()>=0) {
						toAllocate = Env.ZERO.max(qtyAvailable.min(toAllocate.max(needsToBeToAllocated[0])));
					}
					if (toAllocate.intValue()>0) {
						log.info("Allocating " + product.getValue() + " : " + product.getName() + " Avail: " + qtyAvailable + " On hand: " + onHand + " To alloc: " + toAllocate);
						log.info(lines.size() + " lines to allocate.");
					} else {
						log.info("Deallocating " + product.getValue() + " : " + product.getName() + " qty " + toAllocate);
					}
				} else {
					continue;
				}
				
				allocated = BigDecimal.ZERO;
				
				// When we are here we know what product, qty available and we have the lines
				// that need to be allocated.
				String logLine;
				for (Iterator<MOrderLine> it2 = lines.iterator(); it2.hasNext(); ) {
					line = it2.next();
	
					// Calculate what to allocate (what we want)
					lineAllocate = line.getQtyOrdered().subtract(line.getQtyDelivered()).subtract(line.getQtyAllocated());
					willAllocate = lineAllocate.min(toAllocate);
					if (willAllocate.signum()!=0) {
						willAllocate = line.allocateOnHand(willAllocate, trxName);
						allocated = allocated.add(willAllocate);
						toAllocate = toAllocate.subtract(willAllocate);
						logLine = "Allocated " + willAllocate + " of " + product.getValue() + " : " + product.getName() + " to order " + line.getC_Order().getDocumentNo() + " " + toAllocate + " left to allocate.";
						if (p!=null)
							p.addLog(logLine);
						else
							log.info(logLine);
						if (toAllocate.equals(BigDecimal.ZERO))
							break;
					} else {
						log.info("Skipping allocation of order " + line.getC_Order().getDocumentNo());
						continue;
					}
				}
			} // Only try to allocate items that are available
			
		}
		return("");
	}
	
	
	/**
	 * Finds all order lines that contains not yet delivered physical items of a specific product.
	 * 
	 * @param productId			The product id being allocated
	 * @param	orderID			If this is supplied (can be zero) this order is also considered
	 * 							for allocation. This mechanism exists because the order might be transferring from
	 * 							status drafted when this function is called.
	 * @param needsToBeToAllocated optional parameter sent by reference (via array) to return total number that needs to be
	 * 							allocated. Null if not used
	 * @param trxName			Transaction name
	 * @return  Order lines to allocate products to.
	 * @throws SQLException
	 */
	public static List<MOrderLine> getOrderLinesToAllocate(int productId, int orderID, BigDecimal[] needsToBeToAllocated, String trxName) throws SQLException {
		String 
		query = "SELECT C_OrderLine.*, C_OrderLine.qtyordered-C_OrderLine.qtydelivered-C_OrderLine.qtyallocated AS needsToAllocate from C_OrderLine \n" + 
			   "JOIN C_Order ON C_OrderLine.C_Order_ID=C_Order.C_Order_ID \n" + 
			   "INNER JOIN C_DocType dt ON dt.C_DocType_ID = C_Order.C_DocType_ID \n" +
			   "WHERE \n" +
			   "dt.DocSubTypeSO != '" + MDocType.DOCSUBTYPESO_Proposal + "' AND \n" +
			   " C_Order.IsSOTrx='Y' AND \n" +
			   "(C_Order.DocStatus IN ('IP','CO', 'IN') OR C_Order.C_Order_ID=?) \n" + 
			   "AND QtyAllocated<>(QtyOrdered-QtyDelivered) AND (QtyReserved>0 OR QtyReserved<QtyAllocated)  \n" + 
			   "AND C_OrderLine.M_Product_ID=? \n" + 
			   "ORDER BY PriorityRule, C_OrderLine.Created ";

		List<MOrderLine> result = new ArrayList<MOrderLine>();
		Properties ctx = Env.getCtx();
		MOrderLine line;
		CPreparedStatement ps = DB.prepareStatement(query, trxName);
		ps.setInt(1, orderID);
		ps.setInt(2, productId);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			if (needsToBeToAllocated!=null)
				needsToBeToAllocated[0] = needsToBeToAllocated[0].add(rs.getBigDecimal("needsToAllocate"));
			line = new MOrderLine(ctx, rs, trxName);
			result.add(line);
		}
		try {
			rs.close();
		} catch (Exception e) {}
		ps.close();
		return(result);
	}
	
	
	/**
	 * Finds all products that can be allocated. A product can be allocated if there are more items 
	 * on hand than what is already allocated. To be allocated the item must also be in demand
	 * (reserved > allocated)
	 * 
	 * @param 	warehouseID		Must be supplied
	 * @param	orderID			If this is supplied (greater than zero) this order's products are the only one considered
	 * 							for allocation. Set to zero if not used
	 * @param   productID		If this is supplied (greater than zero) then only this productID will be used disregarding everything else. 
	 * 							Set to zero if not used  							
	 * @param	trxName			Transaction
	 * @return
	 * @throws 	SQLException
	 */
	public static List<StockInfo> getProductsToAllocate(int warehouseID, int orderID, int productID, String trxName) throws SQLException {
		
		List<StockInfo> result = new ArrayList<StockInfo>();
		StockInfo si;
		String query1 =
				
				"select  ol.m_warehouse_id, " +  
				"ol.m_product_id, " +  
				"(select sum(qtyonhand) from m_product_stock_v sv where m_product_id=ol.m_product_id and m_warehouse_id=ol.m_warehouse_id ) as qtyonhand, " +  
				"sum(ol.qtyreserved) as qtyreserved " + 
			    ", sum(ol.qtyallocated) as qtyallocated " + 
			    "from " +  
			    "c_orderline ol " + 
			    "join c_order o on (ol.c_order_id=o.c_order_id) " + 
			    "join c_doctype dt on (o.c_doctype_id=dt.c_doctype_id) " +
			    "join m_product p on (ol.m_product_id=p.m_product_id) " +
			    "where o.isSoTrx='Y' " +
			    "and dt.DocSubTypeSO != ? " +
			    "and (o.docstatus in ('CO','DR','IP') ";
			    if (orderID!=0) {
			    	query1 += " or o.c_order_id=? ";
			    }
			    query1 += ") and (ol.qtydelivered<>ol.qtyordered or ol.qtyordered<ol.qtyallocated)";
				if (warehouseID!=0) {
					query1 += " and ol.m_warehouse_id=? ";
				}
				if (productID!=0) {
					query1 += " and ol.m_product_id=? ";
				}
		query1 +=
			    "group by ol.m_warehouse_id, ol.m_product_id " + 
			    "having " +
			    // This last criteria show only products that can be allocated
			    "(((select sum(qtyonhand) from m_product_stock_v sv where m_product_id=ol.m_product_id and m_warehouse_id=ol.m_warehouse_id ) > sum(ol.qtyallocated)) " +
			    "or (sum(ol.qtyreserved) < sum(ol.qtyallocated)))";
			    

		CPreparedStatement ps =  DB.prepareStatement(query1, trxName);
		int i = 1;
		ps.setString(i++, MDocType.DOCSUBTYPESO_Proposal);
		
		if (orderID!=0)
			ps.setInt(i++, orderID);
		if (warehouseID!=0)
			ps.setInt(i++, warehouseID);
		if (productID!=0)
			ps.setInt(i++, productID);
		

		ResultSet rs;
		try {
			rs = ps.executeQuery();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		while(rs.next()) {
			si = new StockInfo();
			si.warehouseId = rs.getInt(1);
			si.productId = rs.getInt(2);
			si.qtyOnHand = rs.getBigDecimal(3);
			si.qtyReserved = rs.getBigDecimal(4);
			si.qtyAvailable = si.qtyOnHand.subtract(si.qtyReserved);
			si.qtyAllocated = rs.getBigDecimal(5);
			result.add(si);
		}
		rs.close();
		ps.close();
		return(result);
	}
	
	

}
