package se.idempiere.deliverypolicy.process;

import java.util.logging.Level;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.compiere.util.CLogger;

/**
 * Overrides InOutGenerate
 * 
 * @author daniel.tamm
 *
 */
public class DeliveryPolicyProcessFactory implements IProcessFactory {

	private final static CLogger log = CLogger.getCLogger(DeliveryPolicyProcessFactory.class);	
	
	@Override
	public ProcessCall newProcessInstance(String className) {
		
		ProcessCall process = null;

		// Override org.compiere.process.InOutGenerate
		if ("org.compiere.process.InOutGenerate".equals(className)) {
			
			className = InOutGenerate.class.getCanonicalName();
			
			//Get Class
			Class<?> processClass = null;
			//use context classloader if available
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			if (classLoader != null)
			{
				try
				{
					processClass = classLoader.loadClass(className);
				}
				catch (ClassNotFoundException ex)
				{
					if (log.isLoggable(Level.FINE))log.log(Level.FINE, className, ex);
				}
			}
			if (processClass == null)
			{
				classLoader = this.getClass().getClassLoader();
				try
				{
					processClass = classLoader.loadClass(className);
				}
				catch (ClassNotFoundException ex)
				{
					log.log(Level.WARNING, className, ex);
					return null;
				}
			}

			if (processClass == null) {
				return null;
			}

			//Get Process
			try
			{
				process = (ProcessCall)processClass.newInstance();
			}
			catch (Exception ex)
			{
				log.log(Level.WARNING, "Instance for " + className, ex);
				return null;
			}
			
			
		}
		
		return process;
		
	}

}
