package se.idempiere.deliverypolicy.validator;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;

import se.idempiere.deliverypolicy.Activator;
import se.idempiere.deliverypolicy.model.MOrderLine;
import se.idempiere.deliverypolicy.process.SalesOrderAllocation;

/**
 * Validator that runs order allocation.
 * 
 * 
 * @author Daniel Tamm
 *
 */
public class MOrderValidator implements ModelValidator {

	private int		m_AD_Client_ID = -1;
	
	private CLogger log = CLogger.getCLogger(this.getClass());	
	
	
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		if (client!=null) m_AD_Client_ID = client.getAD_Client_ID();

		// Look for changes on order line
		// Don't add this for now. See comments on model change.
		engine.addModelChange(MOrderLine.Table_Name, this);
		
		// Run allocation on prepare
		engine.addDocValidate(MOrder.Table_Name, this);
		log.info(this.getClass() + " initialized.");
		System.out.println("Init client id = " + m_AD_Client_ID);
	}

	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		
		System.out.println("Login MOrderValidator");
		
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		
		// Make sure we have settings that needs allocation
		boolean salesOrderAllocationEnabled = MSysConfig.getBooleanValue(Activator.SALES_ORDERLINE_ALLOCATION, false, po.getAD_Client_ID());
		if (!salesOrderAllocationEnabled)
			return null;

		org.compiere.model.MOrderLine ol = (org.compiere.model.MOrderLine)po;
		MOrder o = (MOrder)ol.getC_Order();
		// Only process sales transactions
		if (!o.isSOTrx())
			return null;
		
		// It's only on delete that we need to re-run allocation to reallocate
		// However, currently you can't delete a line that has reservations
		// This check is then unneccessary. Everything should happen at prepare.
		if (type == ModelValidator.CHANGETYPE_DELETE) {
			
		}
		
		return null;
		
	}

	/**
	 * If document is prepared, run allocation after prepare.
	 */
	@Override
	public String docValidate(PO po, int timing) {
		
		// Make sure we have settings that needs allocation
		boolean salesOrderAllocationEnabled = MSysConfig.getBooleanValue(Activator.SALES_ORDERLINE_ALLOCATION, false, po.getAD_Client_ID());
		if (!salesOrderAllocationEnabled)
			return null;

		MOrder o = (MOrder)po;
		if (!o.isSOTrx())
			return null;
		
		// If non-binding proposal, don't do any allocation
		if (MDocType.DOCSUBTYPESO_Proposal.equals(o.getC_DocType().getDocSubTypeSO())) {
			return null;
		}
		
		if (timing == ModelValidator.TIMING_AFTER_PREPARE) {
			try {
				SalesOrderAllocation.runSalesOrderAllocation(null, log, po.getCtx(), o.getM_Warehouse_ID(), o.get_ID(), 0, o.get_TrxName());
			} catch (Exception e) {
				throw new AdempiereException(e);
			}
		}
		
		return null;
	}

}
